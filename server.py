from flask import Flask, render_template, request, session, redirect, url_for
from markupsafe import escape
import json
import hashlib

# git add --all
# git commit -am "[msg]"
# git push

app = Flask(__name__)
app.secret_key = b'R\x08\xf4\xbd8\x03\x9b\x15\x89(\x13\x02F\x8b"c'


@app.route('/home', methods=['GET'])
def accueil():
    return render_template('index.html')

@app.route('/userlist', methods=['GET'])
def userlist():
    if session['id'] == 1 or session['id'] == 2:
        file = open('users.json', 'r')
        data = json.load(file)
        return render_template('userlist.html', **locals())
    return redirect(url_for('accueil'))

@app.route('/reset', methods=['GET'])
def reset():
    if session['id'] >=3:
        return redirect(url_for('accueil'))
    file = open('users.json', 'w')
    newdata=[
        {
        "nom": "Gries",
        "prenom": "Maxime",
        "mail": "griesmaxime2@gmail.com",
        "age": 16,
        "password": hashlib.sha224(b"MonMotDePasse").hexdigest(),
        "id": 1
        }, {
        "prenom": "Ludovic",
        "nom": "Tison",
        "age": 22,
        "mail": "joliot10.ludovic.tison@gmail.com",
        "password": hashlib.sha224(b"22AnsCestJeune").hexdigest(),
        "id": 2
        }, {
        "prenom": "Tony",
        "nom": "Stark",
        "age": 34,
        "mail": "tony.stark@gmail.com",
        "password": hashlib.sha224(b"TonyStark").hexdigest(),
        "id": 3
        }
    ]
    json.dump(newdata, file)
    if session['mail'] == 'griesmaxime2@gmail.com' or session['mail'] == 'joliot10.ludovic.tison@gmail.com' or session['mail'] == 'tony.stark@gmail.com':
        return redirect(url_for('userlist'))
    else:
        return redirect(url_for('logout'))

@app.route('/delete/<mail>')
def delete(mail):
    if session['id']:
        if session['id'] == 1 or session['id'] == 2:
            data = json.load(open('users.json', 'r'))
            for user in data:
                if mail == user['mail']:
                    data.remove(user)
                    file = open('users.json', 'w')
                    json.dump(data, file)
                    if mail == session['mail']:
                        return redirect(url_for('logout'))
                    return redirect(url_for('userlist'))

@app.route('/login', methods=['POST', 'GET'])
def adduser():
    res = ''
    if request.method == 'POST':
        res = 'Utilisateur introuvable'
        for user in json.load(open('users.json', 'r')):
            testpass = request.form['password']
            if request.form['email'] == user['mail']:
                if hashlib.sha224(testpass.encode('utf-8')).hexdigest() == user['password']:
                    session['nom'] = user['nom']
                    session['prenom'] = user['prenom']
                    session['age'] = user['age']
                    session['mail'] = user['mail']
                    session['id'] = user['id']
                    return redirect(url_for('me'))
                res = 'Mot de passe incorrect'
                mail = request.form['email']
        return render_template('login.html', **locals())

    return render_template('login.html', **locals())

@app.route('/register', methods=['POST', 'GET'])
def register():
    if request.method == 'POST':
        nom = request.form['nom']
        prenom = request.form['prenom']
        age = int(request.form['age'])
        mail = request.form['mail1']
        confmail = request.form['mail2']
        passw = request.form['pass1']
        confpass = request.form['pass2']
        errors = 0
        already = ''
        errormail = ''
        errorpass = ''
        for user in json.load(open('users.json', 'r')):
            if user['mail'] == mail:
                already = 'Cet e-mail existe déjà.'
                errors += 1
        if mail != confmail:
            errormail = 'Les deux e-mails ne correspondent pas.'
            errors += 1
        if passw != confpass:
            errorpass = 'Les deux mots de passe ne correspondent pas.'
            errors += 1
        if errors >= 1:
            return render_template('register.html', **locals())
        else:
            userinfo = {
                'prenom': prenom,
                'nom': nom,
                'age': age,
                'mail': mail,
                'password': hashlib.sha224(passw.encode('utf-8')).hexdigest(),
                'id': len(json.load(open('users.json', 'r')))+1
            }

            def changeid(id):
                if id == 1 or id == 2:
                    id += 1
                    changeid(id)
                data = json.load(open('users.json', 'r'))
                for user in data:
                    if id == user['id']:
                        id += 1
                        changeid(id)

            changeid(userinfo['id'])

            session['prenom'] = prenom
            session['nom'] = nom
            session['age'] = age
            session['mail'] = mail
            session['id'] = userinfo['id']
            
            data = json.load(open('users.json', 'r'))
            data.append(userinfo)
            filewrite = open('users.json', 'w')
            json.dump(data, filewrite)
            return redirect(url_for('me'))

    return render_template('register.html', **locals())

@app.route('/me')
def me():
    if 'nom' in session:
        return render_template('me.html', **locals())
    return redirect(url_for('adduser', **locals()))

@app.route('/logout')
def logout():
    session.pop('nom', None)
    session.pop('prenom', None)
    session.pop('age', None)
    session.pop('mail', None)
    session.pop('id', None)
    return redirect(url_for('accueil'))


app.run(host='0.0.0.0', port='5000', debug=True)