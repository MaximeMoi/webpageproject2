## points demandés:
 * une page d'accueil avec un bouton pour naviguer vers une page qui affichera une table contenant différents champs que vous choisirez de 2 types différents
 * La table en question devra déjà contenir 3 lignes complètes
 * On doit pouvoir supprimer chaque élément de la table
 * La page avec la table doit contenir un bouton pour réinitialiser le contenu de la table tel qu'il était au départ
 * la page avec la table doit contenir un lien vers une page avec un formulaire
 * Ce formulaire doit servir à remplir la table

## Liens vers des exemples

 * Exemple minimal: https://projet-nsi1.tk/index.html
 * Exemple amélioré: https://projet-nsi2.tk/index.html